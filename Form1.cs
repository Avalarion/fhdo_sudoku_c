using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    /// <summary>
    /// Diese Klasse dient als SudokuSpiel Oberfläche und Logik
    /// </summary>
    public partial class Form1 : Form
    {
        private const int DIMENSIONS = 9;
        private const int FIELD_SIZE = 30;
        private const int FIELD_BORDER = 5;
        private Color WARNCOLOR = Color.Red;
        private Color DEFAULTCOLOR = Color.Black;
        private RichTextBox[,] fields;
        private Button checkButton;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int startX = FIELD_BORDER;
            int currentX = startX;
            int startY = FIELD_BORDER;
            int currentY = startY;
            fields = new RichTextBox[DIMENSIONS, DIMENSIONS];
            for (int i = 0; i < DIMENSIONS; i++)
            {
                for (int j = 0; j < DIMENSIONS; j++)
                {
                    fields[i, j] = new RichTextBox();
                    fields[i, j].MaxLength = 1;
                    fields[i, j].Multiline = false;
                    //fields[i, j].Text = i.ToString() + "," + j.ToString();
                    fields[i, j].SelectionAlignment = HorizontalAlignment.Center;
                    this.Controls.Add(fields[i, j]);
                    //fields[i, j].TextChanged += new System.EventHandler(this.textChanged);
                    fields[i, j].SetBounds(currentX, currentY, FIELD_SIZE, FIELD_SIZE);
                    currentX += FIELD_BORDER + FIELD_SIZE;
                    if ((j + 1) % 3 == 0) currentX += FIELD_BORDER * 2;
                }
                currentX = startX;
                currentY += FIELD_BORDER + FIELD_SIZE;
                if ((i + 1) % 3 == 0) currentY += FIELD_BORDER * 2;
            }

            checkButton = new Button();
            this.Controls.Add(checkButton);
            checkButton.SetBounds(currentX, currentY, (startX + 9 * (FIELD_BORDER + FIELD_SIZE) + FIELD_BORDER), FIELD_SIZE);
            checkButton.Text = "Überprüfen";
            checkButton.Click += new System.EventHandler(this.checkGame);
            currentY += FIELD_SIZE + FIELD_BORDER;

            currentY += FIELD_BORDER;
            this.Text = "SUDOKU - Bastian Bringenberg";
            this.Width = startX + 9 * (FIELD_BORDER + FIELD_SIZE) + FIELD_BORDER * 2 + FIELD_BORDER * 3 * 2;
            this.Height = currentY + FIELD_SIZE;
            loadGame();
        }

        /// <summary>
        /// Die Methode um ein neues Spiel zu laden.
        /// </summary>
        private void loadGame() {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "sudoku files (*.sudoku)|*.sudoku";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                String[,] csvFields = GDK.CSV.read(fd.FileName);
                for (int i = 0; i < DIMENSIONS; i++)
                {
                    for (int j = 0; j < DIMENSIONS; j++) {
                        if (csvFields[i, j] != "x")
                        {
                            fields[i, j].Text = csvFields[i, j];
                            fields[i, j].Enabled = false;
                        }
                        else
                        {
                            fields[i, j].Text = "";
                            fields[i, j].Enabled = true;
                        }
                    }
                }
            }
            else loadGame();

        }

        /// <summary>
        /// Die Methode um ein Spiel auf Korrektheit zu überprüfen.
        /// Die Parameter werden nicht zur Logik, sondern von dem EventHandler benötigt.
        /// </summary>
        private void checkGame(object sender, EventArgs e)
        {
            Boolean foundError = false;
            //Check Rows
            Boolean[] checkedFields = new Boolean[DIMENSIONS];
            for (int i = 0; i < DIMENSIONS; i++)
            {
                for (int j = 0; j < DIMENSIONS; j++)
                {
                    if (fields[i, j].Text.Trim() != "")
                    {
                        int number = Convert.ToInt32(fields[i, j].Text);
                        if (number < 1 || number > DIMENSIONS)
                        {
                            foundError = true;
                        }
                        else
                        {
                            checkedFields[number - 1] = true;
                        }
                    }
                    else
                    {
                        foundError = true;
                    }
                }
                for (int j = 0; j < DIMENSIONS; j++)
                {
                    if (!checkedFields[j]) foundError = true;
                }
                checkedFields = new Boolean[DIMENSIONS];
            }

            // Load Colms
            checkedFields = new Boolean[DIMENSIONS];
            for (int i = 0; i < DIMENSIONS; i++)
            {
                for (int j = 0; j < DIMENSIONS; j++)
                {
                    if (fields[j, i].Text.Trim() != "")
                    {
                        int number = Convert.ToInt32(fields[j, i].Text);
                        if (number < 1 || number > DIMENSIONS)
                        {
                            foundError = true;
                        }
                        else
                        {
                            checkedFields[number - 1] = true;
                        }
                    }
                    else
                    {
                        foundError = true;
                    }
                }
                for (int j = 0; j < DIMENSIONS; j++)
                {
                    if (!checkedFields[j]) foundError = true;
                }
                checkedFields = new Boolean[DIMENSIONS];
            }

            // Load Blocks
            // @TODO: Fix Number to DIMENSIONS
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    //eachBox
                    for (int x = 0; x < 3; x++) {
                        for (int y = 0; y < 3; y++)
                        {
                            String fieldInput = fields[i*3+x,j*3+y].Text;
                            if (fieldInput.Trim() != "")
                            {
                                int number = Convert.ToInt32(fieldInput);
                                if (number < 1 || number > DIMENSIONS)
                                {
                                    foundError = true;
                                }
                                else
                                {
                                    checkedFields[number - 1] = true;
                                }
                            }
                            else
                            {
                                foundError = true;
                            }
                        }
                    }
                    for (int x = 0; x < DIMENSIONS; x++)
                    {
                        if (!checkedFields[x]) foundError = true;
                    }
                    checkedFields = new Boolean[DIMENSIONS];

                }
            }

            if (!foundError) {
                MessageBox.Show("Herzlichen Glückwunsch, Sie haben gewonnen!");
                this.loadGame();
            }else {
                MessageBox.Show("Leider wurde das Sudoku nicht korrekt gelöst.");
            }
        }
    }
}
